import model.Customer;
import model.Account;

public class App {
    public static void main(String[] args) throws Exception {

        Customer customer1 = new Customer(1, "Tam1", 0);
        Customer customer2 = new Customer(2, "Tam2", 0);
        System.out.println(customer1);
        System.out.println(customer2);

        Account account1 = new Account(1, customer1);
        Account account2 = new Account(1, customer1, 200.00);
        System.out.println(account1);
        System.out.println(account2);

    }
}
